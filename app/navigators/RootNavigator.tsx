import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../screens/Login/LoginScreen';
import TabNavigator from './TabNavigator';

export default createStackNavigator({
  Login: {
    screen: LoginScreen,
  },
  Tab: TabNavigator,
}, {
  headerMode: 'none',
  initialRouteName: 'Login',
});
