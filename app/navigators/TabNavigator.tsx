import React from 'react'
import { createBottomTabNavigator } from 'react-navigation';
import LocationScreen from '../screens/Location/LocationScreen';
import HoursScreen from '../screens/Hours/HoursScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../utils/colors';


export default createBottomTabNavigator({
  Location: {
    screen: LocationScreen,
  },
  Hours: {
    screen: HoursScreen,
  },
}, {
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Location') {
        iconName = 'location-arrow';
      } else if (routeName === 'Hours') {
        iconName = 'hourglass-start';
      }
      
      return <Icon name={iconName} size={20} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: colors.violet,
    inactiveTintColor: 'gray',
  },
});
