import axios from 'axios';
import { HARVEST_ROOT_URL } from '../config';

export default {
  get: (path:string, accessToken:string) => {
    return axios.get(
      `${HARVEST_ROOT_URL}${path}`,
      {
        headers: {
          'User-Agent': 'Node.js Harvest API Sample',
          'Harvest-Account-ID': '542650',
          'Content-type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${accessToken}`,
        }
      }
    )
  },

  post: (path:string, body:any, accessToken:string) => {
    return axios.post(
      `${HARVEST_ROOT_URL}${path}`,
      body,
      {
        headers: {
          'User-Agent': 'Node.js Harvest API Sample',
          'Harvest-Account-ID': '542650',
          'Content-type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${accessToken}`,
        }
      }
    )
  },

  patch: (path:string, body:any, accessToken:string) => {
    return axios.patch(
      `${HARVEST_ROOT_URL}${path}`,
      body,
      {
        headers: {
          'User-Agent': 'Node.js Harvest API Sample',
          'Harvest-Account-ID': '542650',
          'Content-type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${accessToken}`,
        }
      }
    )
  }

}