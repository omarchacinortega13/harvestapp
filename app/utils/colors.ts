export default {
  white: '#fff',
  darkBlue: '#2c3e50',
  gray: '#454D52',
  orange: '#F9F5ED',
  yellow: '#F0C56F',
  green: '#D4F2D3',
  violet: '#6C55AB',
  beige: '#F9F5ED'
};
