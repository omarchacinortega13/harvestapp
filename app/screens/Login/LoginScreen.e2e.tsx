import React, { PureComponent } from 'react';
import {
 View, Text, TextInput, Button, StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import { login } from '../../redux/authReducer';
import colors from '../../utils/colors';

interface Props {
  navigation: NavigationScreenProp<{}>,
  login: typeof login,
}

interface State {
  accessToken: string,
}

class LoginScreen extends PureComponent<Props, State> {

  constructor(props:Props) {
    super(props);
    this.state = {
      accessToken: '',
    };
  }

  onChangeUsername = (accessToken:string) => this.setState({ accessToken })
  onLoginPressed = () => {
    this.props.login(this.state.accessToken)
    this.props.navigation.navigate({routeName: 'Tab'})
  }

  render() {
    return (
      <View style={styles.container}>        
        <Text style={styles.title}>Access Token</Text>
        <TextInput
          testID="txt.authToken"
          style={styles.accessToken}
          onChangeText={this.onChangeUsername}
          value={this.state.accessToken}
          secureTextEntry={false}
        />
        <Button
          testID="btn.login"
          title="Login"
          onPress={this.onLoginPressed}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  location: state.location,
  auth: state.auth,
});


export default connect(mapStateToProps, { login })(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.beige,
  },
  accessToken: {
    textAlign: 'center',
    alignSelf: 'stretch',
    margin: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    height: 20,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 20,
    color: colors.gray,
    fontWeight: 'bold',
  },  
});
