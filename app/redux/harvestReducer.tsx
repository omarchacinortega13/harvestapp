import axios from 'axios';
import harvestService from '../utils/harvestService';

export const FETCH_HOURS = 'FETCH_HOURS';
export const FETCH_HOURS_SUCCESS = 'FETCH_HOURS_SUCCESS';
export const FETCH_HOURS_FAILED = 'FETCH_HOURS_FAILED';
export const SET_CURRENT_TASK = 'SET_CURRENT_TASK';
export const CLEAN_CURRENT_TASK = 'CLEAN_CURRENT_TASK';

export interface HoursActions {
  type: typeof FETCH_HOURS_SUCCESS | typeof FETCH_HOURS_FAILED
  hours: Object,
  error: any,  
}

export interface TaskActions {
  type: typeof SET_CURRENT_TASK | typeof CLEAN_CURRENT_TASK,
  task: Object
}

export function fetchHoursSuccess(hours:Object) {
  return {
    type: FETCH_HOURS_SUCCESS,
    hours
  }
}

export function fetchHoursFailed(error:any) {
  return {
    type: FETCH_HOURS_FAILED,
    error
  }
}

export function setCurrentTask(task: object) {
  return {
    type:  SET_CURRENT_TASK,
    task   
  }
}

export function cleanCurrentTask() {
  return {
    type:  CLEAN_CURRENT_TASK,
  }
}

export function fetchHours() {
  return ((dispatch: Function, getState: Function) => {
    const { auth } = getState();
    const { accessToken } = auth

    dispatch({ type: FETCH_HOURS });
    harvestService.get('/v2/time_entries?user_id=2026476', accessToken)
    .then(response => dispatch(fetchHoursSuccess(response.data)))
    .catch(error => dispatch(fetchHoursFailed(error)))    
  })
}

export type HarvestAction = 
HoursActions | TaskActions

export default function (state = [], action:HarvestAction) {
  switch (action.type) {
    case FETCH_HOURS_SUCCESS:
      return {
        ...state,
        hours: action.hours,
        error: null,
      };
    case FETCH_HOURS_FAILED:
      return {
        ...state,
        hours: null,
        error: action.error,
      };
    case SET_CURRENT_TASK:
      return {
        ...state,
        currentTask: action.task
      };
    case CLEAN_CURRENT_TASK:
      return {
        ...state,
        currentTask: null
      };
    default:
      return state;
  }
}
