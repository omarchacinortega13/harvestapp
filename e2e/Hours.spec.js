/* globals describe beforeAll afterAll beforeEach afterEach it */
import {
  device, expect, element, by,
} from 'detox';
import harvest from './harvest';
import timeEntries from './fixtures/time_entries';
import usersMe from './fixtures/users/me';

describe('Login', () => {
  beforeAll(async () => {
    await harvest.listen();
  });

  afterAll(async () => {
    await harvest.close();
  });

  beforeEach(() => {
    harvest.reset();
  });

  afterEach(() => {
    harvest.reset();
  });

  it('Should show hours from the server', async () => {
    await device.launchApp({ permissions: { location: 'always' } });

    await element(by.id('btn.login')).tap();
    harvest.expect('/v2/users/me')
      .withResponse(usersMe);

    await element(by.id('tabbar.hours')).tap();

    harvest.expect('/v2/time_entries?user_id=2026476')
      .withResponse(timeEntries);

    await expect(element(by.id('running')
      .withAncestor(by.id('time-entry-1'))))
      .toBeVisible();
    await expect(element(by.id('not-running')
      .withAncestor(by.id('time-entry-2'))))
      .toBeVisible();
    await expect(element(by.id('not-running')
      .withAncestor(by.id('time-entry-3'))))
      .toBeVisible();
  });
});
