export default {
  id: 2026476,
  first_name: 'Firstname',
  last_name: 'Lastname',
  email: 'someone@somewhere.com',
  created_at: '2015-10-27T08:04:23Z',
  updated_at: '2017-12-18T10:24:32Z',
};