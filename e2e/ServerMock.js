import proxy from 'express-http-proxy';
import url from 'url';
import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';

class Expect {
  constructor({
    method,
    pattern,
    requestBody,
    responseBody,
    headers,
    status,
  }) {
    this.status = status || 200;
    this.method = method || 'GET';
    this.pattern = pattern;
    this.requestBody = requestBody;
    this.responseBody = responseBody;
    this.headers = headers || {};
    this.calls = []
    this.nextCall = new Promise((resolve) => { this.resolveNextCall = resolve });
  }

  toString() {
    return `Expect ${this.method} ${this.pattern} ${this.responseBody && JSON.stringify(this.responseBody, 2) || ''}`;
  }

  withResponse(responseBody) {
    this.responseBody = responseBody;
    return this;
  }

  withHeaders(headers) {
    this.headers = headers;
    return this;
  }

  withStatus(status) {
    this.status = status;
    return this;
  }

  matches(req) {
    if (req.method === this.method) {
      if ((typeof this.pattern === 'string' && req.originalUrl === this.pattern) ||
        (this.pattern instanceof RegExp && req.originalUrl.match(this.pattern))) {
        if (this.requestBody === undefined || this.requestBody === req.body) {
          return true;
        }
      }
    }
    return false;
  }

  handle(req, res) {
    if (typeof this.responseBody === 'function') {
      res.status(this.status)
      this.responseBody(req, res)
    } else {
      res.status(this.status).send(this.responseBody);
    }
    this.calls.push({
      url: req.originalUrl,
      body: req.body,
      headers: req.headers,
    })

    this.resolveNextCall(this.calls[this.calls.length - 1])
    this.nextCall = new Promise((resolve) => { this.resolveNextCall = resolve });
  }
}

export default class ServerMock {
  constructor(name, targetHost, targetPort, proxyPort, proxyMode) {
    this.name = name;
    this.targetHost = targetHost;
    this.targetPort = targetPort;
    this.proxyPort = proxyPort;
    this.proxyMode = proxyMode || 'proxy.missing' // proxy.all / proxy.missing / proxy.none
    this.debug = false
    this.expects = [];
    this.pendingRequests = [];
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(this.middleware);
  }

  reset() {
    this.retryPending()
    const numberOfPendingRequests = this.pendingRequests.length;
    if (numberOfPendingRequests) {
      const errorMessages = this.failPending();
      const registeredExpects = this.expects.map(e => e.toString()).join('\n');
      this.pendingRequests = [];
      this.expects = [];
      throw new Error(`${numberOfPendingRequests} unhandled requests: \n${errorMessages.join('\n')}\nExpects:\n${registeredExpects}`);
    } else {
      this.expects = [];
    }
  }

  listen() {
    if (!this.mockServer) {
      this.mockServer = http.createServer(this.app);
      this.mockServer.setTimeout(1000);
      console.log(`Creating mock server on ${this.targetPort}`);
      return new Promise((resolve) => {
        this.mockServer.listen(this.targetPort, () => {
          resolve();
        });
      });
    }
    return Promise.resolve();
  }

  close() {
    if (this.debug) return;
    const promises = []

    if (this.mockServer) {
      console.log('Closing mock server...')
      promises.push(new Promise((resolve) => {
        this.mockServer.close(() => {
          console.log('Mock server closed');
          resolve();
        });
        this.mockServer = null;
      }));
    }

    if (this.proxyServer) {
      console.log('Closing proxy server...')
      promises.push(new Promise((resolve) => {
        this.proxyServer.close(() => {
          console.log('Proxy server closed')
          resolve();
        });
        this.proxyServer = null;
      }));
    }

    return Promise.all(promises);
  }

  logMessageForMissingRoute(req, res, resData) {
    let message = '/*** REQUEST ***/\n'
    message += `${this.name}.expect('${req.originalUrl}'${req.method !== 'GET' ? `, '${req.method}'` : ''})`
    if (res) {
      message += `\n  .withStatus(${res.statusCode})`
      message += `\n  .withHeaders(${JSON.stringify(req.headers, 2)})`
      if (resData) {
        if (res.headers['content-type'] && res.headers['content-type'].indexOf('application/json') > -1) {
          const data = JSON.stringify(JSON.parse(resData.toString('utf8'), 2));
          message += `\n  .withResponse(${data})`
        } else {
          message += `\n// Ignored response body of type ${res.headers['content-type']}`
        }
      } else {
        message += '// empty response body'
      }
    }
    return message;
  }

  middleware = (req, res, next) => {
    if (!this.handle(req, res, next)) {
      this.pendingRequests.push([req, res, next])
    }
  }

  retryPending() {
    this.pendingRequests = this.pendingRequests.map((req, index) => {
      if (!this.handle(...req)) {
        return req
      }
    }).filter(val => val !== undefined)
  }

  failPending() {
    return this.pendingRequests.map(([req, res, next]) => {
      const uri = url.parse(req.originalUrl);
      if (this.proxyMode !== 'proxy.none') {
        console.log("Proxying to", this.targetHost, uri.path)
        req.headers['Content-type'] = req.headers['Content-type'] || 'application/json'
        proxy(this.targetHost, {
          https: true,
          userResDecorator: (proxyRes, proxyResData /* , userReq, userRes */) => {
            console.log(this.logMessageForMissingRoute(req, proxyRes, proxyResData))
            return proxyResData
          }
        })(req, res, next)
      }

      return this.logMessageForMissingRoute(req)
    });
  }

  handle(req, res, next) {
    const matching = this.expects.filter(e => e.matches(req))
    const uri = url.parse(req.originalUrl);
    if (matching.length > 1) {
      matching.forEach(e => console.warn("Duplicate match", e.toString()))
    }
    if (
      matching.length
      && (!uri.host || uri.host === this.targetHost)
      && (this.proxyMode !== 'proxy.all')
    ) {
      console.log('/* handling', req.method, req.originalUrl, '*/')
      matching[matching.length - 1].handle(req, res);

      return true;
    }
    console.log(`couldn't find handler for ${uri.path}...`)
    return false;
  }

  expect(pattern, method) {
    const e = new Expect({pattern, method});
    console.log(`Adding ${e.toString()}`);
    this.expects.push(e);
    setTimeout(() => {
      this.retryPending()
    }, 0);
    return e;
  }
}