import ServerMock from './ServerMock';

const PROXY_MODE = process.env.PROXY_MODE || 'proxy.missing';

const harvest = new ServerMock('harvest', 'api.harvestapp.com', 8080, 3000, PROXY_MODE);
harvest.debug = process.env.DEBUG;

export default harvest;
