/* globals describe beforeAll afterAll beforeEach afterEach it */
import {
  device, expect, element, by,
} from 'detox';
import harvest from './harvest';
import usersMe from './fixtures/users/me';

describe('Login', () => {
  beforeAll(async () => {
    await harvest.listen();
  });

  afterAll(async () => {
    await harvest.close();
  });

  beforeEach(() => {
    harvest.reset();
  });

  afterEach(() => {
    harvest.reset();
  });

  it('Should take you to the location screen after login', async () => {
    await device.launchApp({ permissions: { location: 'always' } });
    await element(by.id('txt.authToken')).replaceText('token');
    await element(by.id('btn.login')).tap();

    harvest.expect('/v2/users/me')
      .withHeaders({
        authorization: 'Bearer token',
      })
      .withResponse(usersMe);

    await expect(element(by.id('btn.location'))).toBeVisible();
  });
});
