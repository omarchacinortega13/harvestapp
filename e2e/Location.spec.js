/* globals describe beforeAll afterAll beforeEach afterEach it */
import {
  device, expect, element, by, waitFor,
} from 'detox';
import harvest from './harvest';
import usersMe from './fixtures/users/me';
import timeEntries from './fixtures/time_entries';


const locationAtWork = [59.912268, 10.7358196];
const locationAtHome = [62.912268, 10.7358196];

describe('Example', () => {
  beforeAll(async () => {
    await harvest.listen();
  });

  afterAll(async () => {
    await harvest.close();
  });

  beforeEach(async () => {
    harvest.reset();
    harvest.expect('/v2/users/me')
      .withResponse(usersMe);

    // await device.reloadReactNative();
  });

  afterEach(() => {
    harvest.reset();
  });

  describe('Location not granted', () => {
    it('Location should be unavailable', async () => {
      await device.launchApp({ permissions: { location: 'never' } });
      await element(by.id('txt.authToken')).typeText('token');
      await element(by.id('btn.login')).tap();
      await element(by.id('btn.location')).tap();
      await expect(element(by.id('error')))
        .toHaveText('Please allow location permission in order to use the app');
    });
  });


  describe('Location granted / User using the app for first time', () => {
    it('User is at work', async () => {
      await device.launchApp({ permissions: { location: 'always' }, delete: true });
      await element(by.id('txt.authToken')).typeText('token');
      await element(by.id('btn.login')).tap();
      await element(by.id('btn.location')).tap();
      harvest.expect('/v2/time_entries?user_id=2026476')
        .withResponse(timeEntries);
      await device.setLocation(...locationAtWork);
      harvest.expect('/v2/time_entries', 'POST')
        .withStatus(200)
        .withResponse({
          id: 999,
          hours: 0.0,
          created_at: '2017-06-27T16:01:23Z',
          updated_at: '2017-06-27T16:01:23Z',
        });

      await device.setLocation(...locationAtWork);
      await expect(element(by.id('userStatus')))
        .toHaveText('User is online :)');
    });
  });

  describe('Location granted', () => {
    it('User is at work and leaves the office', async () => {
      console.log('User is at work and leaves the office');

      // await element(by.id('btn.login')).tap();
      await device.setLocation(...locationAtWork);
      harvest.expect('/v2/time_entries', 'POST')
        .withResponse({
          id: 999,
        });

      harvest.expect('/v2/time_entries?user_id=2026476')
        .withResponse(timeEntries);

      await waitFor(element(by.label('User is online :)'))).toBeVisible().withTimeout(2000);
      await device.setLocation(...locationAtHome);
      harvest.expect('/v2/time_entries/999/stop', 'PATCH')
        .withResponse({});
      await expect(element(by.id('userStatus')))
        .toHaveText('User is not at work :(');
    });

    it('User is at work, leaves the office and comes back', async () => {
      harvest.expect('/v2/time_entries?user_id=2026476')
        .withResponse(timeEntries);
      await device.setLocation(...locationAtWork);
      await waitFor(element(by.label('User is online :)'))).toBeVisible().withTimeout(2000);
      harvest.expect('/v2/time_entries', 'POST')
        .withResponse({
          id: 999,
        });
      await device.setLocation(...locationAtHome);
      harvest.expect('/v2/time_entries/999/stop', 'PATCH')
        .withResponse({});
      await waitFor(element(by.label('User is not at work :('))).toBeVisible().withTimeout(2000);
      await device.setLocation(...locationAtWork);
      await waitFor(element(by.label('User is online :)'))).toBeVisible().withTimeout(2000);
      await expect(element(by.id('userStatus'))).toHaveText('User is online :)');
    });
  });
});
